ARG PHP_IMAGE
ARG NGINX_IMAGE

FROM ${PHP_IMAGE} AS php-fpm

COPY --chown=gitlab-runner:gitlab-runner app/composer.json app/composer.lock app/symfony.lock ./

USER gitlab-runner

ARG APP_SECRET=db5022847953de2ec2c20922297582461c94b8be

# build for production
ENV APP_ENV=prod \
    APP_SECRET=$APP_SECRET

RUN composer install \
    --no-dev \
    --prefer-dist --no-autoloader --no-scripts --no-progress --no-suggest

# copy only specifically what we need
COPY --chown=gitlab-runner:gitlab-runner app/bin/console bin/
COPY --chown=gitlab-runner:gitlab-runner app/public public/
COPY --chown=gitlab-runner:gitlab-runner app/templates templates/
COPY --chown=gitlab-runner:gitlab-runner app/migrations migrations/
COPY --chown=gitlab-runner:gitlab-runner app/.env ./
COPY --chown=gitlab-runner:gitlab-runner app/config config/
COPY --chown=gitlab-runner:gitlab-runner app/src src/

# prevent caching "composer dump-env prod"
RUN head -c 5 /dev/random > random_bytes && \
    set -eux; \
    echo "Create var/*"; \
    mkdir -p \
        var/cache \
        var/log; \
    composer dump-autoload --no-dev --classmap-authoritative; \
    composer dump-env prod; \
#    composer run-script --no-dev post-install-cmd; \
#    bin/console c:w; \
    chmod +x bin/console; sync

#--------------------------------------------------------
# NGINX
#--------------------------------------------------------
FROM registry.gitlab.com/dia-helper/dia-helper-backend/nginx:882d75d5 AS nginx
COPY --from=php-fpm --chown=nginx:nginx /app/public /app/public/
