.PHONY: pre-commit-check
pre-commit-check:
	cp pre-commit .git/hooks/pre-commit
	chmod +x .git/hooks/pre-commit

.PHONY: init
init: pre-commit-check
	docker-compose down --remove-orphans
	cp app/.env app/.env.local
	docker-compose up -d
	docker-compose exec php-fpm composer install
	docker-compose exec php-fpm bin/console d:m:m -q

.PHONY: restart
restart:
	docker-compose down
	docker-compose up -d

.PHONY: restart
restart-hard:
	docker-compose down --remove-orphans
	docker-compose up -d

.PHONY: cs-fix
cs-fix:
	docker-compose exec php-fpm vendor/bin/php-cs-fixer fix

.PHONY: cs-check
cs-check:
	docker-compose exec php-fpm vendor/bin/php-cs-fixer fix --dry-run

.PHONY: test
test:
	docker-compose exec php-fpm bin/phpunit

.PHONY: phpstan
phpstan:
	docker-compose exec php-fpm vendor/bin/phpstan analyse -c phpstan.neon

.PHONY: check
check: cs-check phpstan