<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210406194212 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create table helper_confirmation_code';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE helper_confirmation_code (id SERIAL NOT NULL, helper_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, is_confirmed BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B99015C9D7693E95 ON helper_confirmation_code (helper_id)');
        $this->addSql('COMMENT ON COLUMN helper_confirmation_code.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN helper_confirmation_code.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE helper_confirmation_code ADD CONSTRAINT FK_B99015C9D7693E95 FOREIGN KEY (helper_id) REFERENCES helper (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE helper_confirmation_code');
    }
}
