<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210406183609 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create table helper';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE helper (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, lat NUMERIC(20, 16) NOT NULL, lng NUMERIC(20, 16) NOT NULL, is_active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_87377BB0444F97DD ON helper (phone)');
        $this->addSql('COMMENT ON COLUMN helper.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN helper.updated_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE helper');
    }
}
