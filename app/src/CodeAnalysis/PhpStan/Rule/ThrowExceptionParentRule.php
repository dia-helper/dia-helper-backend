<?php

declare(strict_types=1);

namespace App\CodeAnalysis\PhpStan\Rule;

use PhpParser\Node;
use PhpParser\Node\Stmt\Throw_;
use PHPStan\Analyser\Scope;
use PHPStan\Rules\Rule;
use PHPStan\Rules\RuleErrorBuilder;

/**
 * @implements Rule<\PhpParser\Node\Stmt\Throw_>
 */
class ThrowExceptionParentRule implements Rule
{
    /** @var string[] */
    private array $validParentList;

    /**
     * @param string[] $validParentList
     */
    public function __construct(array $validParentList)
    {
        $this->validParentList = $validParentList;
    }

    /**
     * {@inheritdoc}
     */
    public function getNodeType(): string
    {
        return Throw_::class;
    }

    /**
     * {@inheritdoc}
     */
    public function processNode(Node $node, Scope $scope): array
    {
        $referencedClasses = $scope->getNativeType($node->expr)->getReferencedClasses();

        foreach ($this->validParentList as $validParent) {
            if (is_a($referencedClasses[0], $validParent, true)) {
                return [];
            }
        }

        return [
            RuleErrorBuilder::message(
                    sprintf(
                        'Invalid Exception parent. You can only throw an exception of one of the following types: %s',
                        implode(', ', $this->validParentList)
                    )
                )
                ->build(),
        ];
    }
}
