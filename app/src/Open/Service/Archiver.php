<?php

declare(strict_types=1);

namespace App\Open\Service;

use App\Open\Exception\FileExtractException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use ZipArchive;

final class Archiver implements ArchiverInterface
{
    private string $projectDir;

    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;
    }

    public function getUnzippedFilePath(UploadedFile $file): string
    {
        $zip = new ZipArchive();
        $filePath = $file->getRealPath();
        if (!$filePath) {
            throw new FileExtractException('file path extract error');
        }
        if (true === $zip->open($filePath)) {
            $unzippedFileDirectory = $this->projectDir . '/var/cache/';
            $zip->extractTo($unzippedFileDirectory);
            $unzippedFilePath = $unzippedFileDirectory . $zip->getNameIndex(0);
            $zip->close();
        } else {
            throw new FileExtractException('file open error');
        }

        return $unzippedFilePath;
    }
}
