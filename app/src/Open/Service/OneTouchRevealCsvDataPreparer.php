<?php

declare(strict_types=1);

namespace App\Open\Service;

use App\Open\DataPreparerInterface;
use App\Open\Dto\ChartDataDto;
use App\Open\Exception\FileExtractException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class OneTouchRevealCsvDataPreparer implements DataPreparerInterface
{
    private const LANG_EN = 'en';

    private const LANG_RU = 'ru';

    private const LANG_BY_EVENT_TYPE_MAP = [
        'Item Type' => self::LANG_EN,
        'Тип события' => self::LANG_RU,
    ];

    private const BLOOD_ROW_KEY_LIST = [
        self::LANG_EN => 'Blood Sugar Reading',
        self::LANG_RU => 'Уровень глюкозы',
    ];

    private const UNITS_INDEX_LIST = [
        self::LANG_EN => 4,
        self::LANG_RU => 3,
    ];

    private const MMOL_L_LABEL_LIST = [
        self::LANG_EN => 'mmol/L',
        self::LANG_RU => 'ммоль/л',
    ];

    private const ACCEPTABLE_MIME_TYPE_LIST = ['text/plain', 'text/csv'];

    private const LOW_VALUE_RAW = 'LO';

    private const LOW_VALUE_INT = 1;

    private const MONTH_TRANSLATE = [
        'jan' => 'янв',
        'feb' => 'фев',
        'mar' => 'мар',
        'apr' => 'апр',
        'may' => 'май',
        'jun' => 'июн',
        'jul' => 'июл',
        'aug' => 'авг',
        'sep' => 'сен',
        'oct' => 'окт',
        'nov' => 'ноя',
        'dec' => 'дек',
    ];

    public function prepareData(UploadedFile $file): ChartDataDto
    {
        $data = [];
        $filePath = $file->getRealPath();
        if (!$filePath) {
            throw new FileExtractException('file path extract error');
        }
        if (($handle = fopen($filePath, 'r')) !== false) {
            $i = 0;
            $lang = self::LANG_EN;
            while (($row = fgetcsv($handle, 1000, ',')) !== false) {
                ++$i;
                if (1 == $i) {
                    $langKey = self::removeBom($row[0]);
                    $lang = self::LANG_BY_EVENT_TYPE_MAP[$langKey] ?? $lang;

                    continue;
                }
                if (empty($row[2]) || self::BLOOD_ROW_KEY_LIST[$lang] != $row[0]) {
                    continue;
                }
                if (self::LANG_RU == $lang) {
                    $dateTimeString = $row[1];
                } else {
                    $dateTimeString = self::formatDate($row[1]);
                }
                $dateTime = new \DateTimeImmutable($dateTimeString, new \DateTimeZone('UTC'));
                $label = $dateTime->getTimestamp() * 1000;
                /** @var string[] $row */
                $rawValue = (float) ((self::LOW_VALUE_RAW == $row[2]) ? self::LOW_VALUE_INT : str_replace(',', '.', $row[2]));
                if (self::MMOL_L_LABEL_LIST[$lang] == $row[self::UNITS_INDEX_LIST[$lang]]) {
                    $value = $rawValue * 18;
                } else {
                    $value = $rawValue;
                }
                $data[$label] = $value;
            }
            fclose($handle);
        }
        ksort($data);

        return new ChartDataDto(array_keys($data), array_values($data));
    }

    public function getAcceptableMimeType(): array
    {
        return self::ACCEPTABLE_MIME_TYPE_LIST;
    }

    private static function formatDate(string $rawDate): string
    {
        $dateWithTranslatedMonth = str_replace(self::MONTH_TRANSLATE, array_keys(self::MONTH_TRANSLATE), $rawDate);

        return preg_replace('/[а-я]+/ui', '', $dateWithTranslatedMonth);
    }

    private static function removeBom(string $rawString): string
    {
        if (substr($rawString, 0, 3) == pack('CCC', 0xef, 0xbb, 0xbf)) {
            $modifiedString = substr($rawString, 3);
        }

        return $modifiedString ?? $rawString;
    }
}
