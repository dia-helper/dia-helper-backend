<?php

declare(strict_types=1);

namespace App\Open\ArgumentResolver;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class OneTouchRevealCsvArgumentResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        if (UploadedFile::class !== $argument->getType()) {
            return false;
        }

        return $request->files->has('file');
    }

    /**
     * @return iterable<UploadedFile>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        yield $request->files->get('file');
    }
}
