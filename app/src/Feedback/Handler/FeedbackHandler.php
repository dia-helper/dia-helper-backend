<?php

declare(strict_types=1);

namespace App\Feedback\Handler;

use App\Feedback\Message\FeedbackMessage;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Email;

class FeedbackHandler implements MessageHandlerInterface
{
    private const SUBJECT = 'Dia-helper feedback';

    private MailerInterface $mailer;

    private string $email;

    public function __construct(MailerInterface $mailer, string $email)
    {
        $this->mailer = $mailer;
        $this->email = $email;
    }

    public function __invoke(FeedbackMessage $message): void
    {
        $email = (new Email())
            ->from($this->email)
            ->to($this->email)
            ->subject(self::SUBJECT)
            ->text(
                'From: ' . $message->getName() . PHP_EOL .
                'Email: ' . $message->getEmail() . PHP_EOL .
                'Message: ' . $message->getMessage() . PHP_EOL
            );
        $file = $message->getFile();
        if ($file) {
            $email->attachFromPath($file->getPath(), $file->getOriginName());
        }

        $this->mailer->send($email);

        if ($file) {
            unlink($file->getPath());
        }
    }
}
