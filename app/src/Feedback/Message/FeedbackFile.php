<?php

declare(strict_types=1);

namespace App\Feedback\Message;

class FeedbackFile
{
    private string $path;

    private string $originName;

    public function __construct(string $path, string $originName)
    {
        $this->path = $path;
        $this->originName = $originName;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getOriginName(): string
    {
        return $this->originName;
    }
}
