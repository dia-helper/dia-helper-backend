<?php

declare(strict_types=1);

namespace App\Feedback\Message;

final class FeedbackMessage
{
    private string $name;

    private string $email;

    private string $message;

    private ?FeedbackFile $file;

    public function __construct(string $name, string $email, string $message, ?FeedbackFile $file)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->file = $file;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getFile(): ?FeedbackFile
    {
        return $this->file;
    }
}
