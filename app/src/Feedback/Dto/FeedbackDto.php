<?php

declare(strict_types=1);

namespace App\Feedback\Dto;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

final class FeedbackDto
{
    public const MAX_FILE_SIZE = '10M';

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private string $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private string $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     */
    private string $message;

    /**
     * @Assert\File(maxSize=FeedbackDto::MAX_FILE_SIZE)
     */
    private ?UploadedFile $file;

    public function __construct(string $name, string $email, string $message, ?UploadedFile $file)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->file = $file;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }
}
