<?php

declare(strict_types=1);

namespace App\Feedback\ArgumentResolver;

use App\Feedback\Dto\FeedbackDto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class FeedbackArgumentResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return FeedbackDto::class === $argument->getType();
    }

    /**
     * @return iterable<FeedbackDto>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $message = $request->get('message');
        $file = $request->files->get('file');

        yield new FeedbackDto($name, $email, $message, $file);
    }
}
