<?php

declare(strict_types=1);

namespace App\Feedback\Controller;

use App\Feedback\Dto\FeedbackDto;
use App\Feedback\Message\FeedbackFile;
use App\Feedback\Message\FeedbackMessage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class FeedbackController extends AbstractController
{
    private ValidatorInterface $validator;

    private MessageBusInterface $messageBus;

    private string $projectDir;

    public function __construct(
        ValidatorInterface $validator,
        MessageBusInterface $messageBus,
        string $projectDir
    ) {
        $this->validator = $validator;
        $this->messageBus = $messageBus;
        $this->projectDir = $projectDir;
    }

    /**
     * @Route("/feedback", methods={"POST"})
     */
    public function __invoke(FeedbackDto $dto): JsonResponse
    {
        /** @var ConstraintViolationList $errors */
        $errors = $this->validator->validate($dto);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;

            return new JsonResponse($errorsString, JsonResponse::HTTP_UNSUPPORTED_MEDIA_TYPE);
        }

        $file = $dto->getFile();
        $tmpFilePath = $file ? $file->getRealPath() : null;
        if (!empty($tmpFilePath)) {
            $filePath = $this->projectDir . '/var/log/' . $file->getFilename();
            move_uploaded_file($tmpFilePath, $filePath);
            $feedbackFile = new FeedbackFile($filePath, $file->getClientOriginalName());
        } else {
            $feedbackFile = null;
        }

        $message = new FeedbackMessage($dto->getName(), $dto->getEmail(), $dto->getMessage(), $feedbackFile);
        $this->messageBus->dispatch($message);

        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }
}
