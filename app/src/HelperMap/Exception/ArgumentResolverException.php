<?php

declare(strict_types=1);

namespace App\HelperMap\Exception;

use App\ErrorProcessing\Exception\ClientException;

class ArgumentResolverException extends ClientException
{
}
