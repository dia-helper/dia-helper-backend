<?php

declare(strict_types=1);

namespace App\HelperMap\Repository;

use App\HelperMap\Entity\Helper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Helper|null findOneBy(array $criteria, array $orderBy = null)
 * @method Helper|null find(int $id, $lockMode = null, $lockVersion = null)
 * @method Helper[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @extends ServiceEntityRepository<Helper>
 */
class HelperRepository extends ServiceEntityRepository implements HelperRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Helper::class);
    }
}
