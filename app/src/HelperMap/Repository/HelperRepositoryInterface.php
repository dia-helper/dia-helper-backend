<?php

namespace App\HelperMap\Repository;

use App\HelperMap\Entity\Helper;

interface HelperRepositoryInterface
{
    /**
     * @param array<string, mixed> $criteria
     * @param string[]             $orderBy
     *
     * @return Helper|null
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @return Helper|null
     */
    public function find(int $id, int $lockMode = null, int $lockVersion = null);

    /**
     * @param array<string, true> $criteria
     * @param string[]            $orderBy
     *
     * @return Helper[]
     */
    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null);
}
