<?php

declare(strict_types=1);

namespace App\HelperMap\Handler;

use App\HelperMap\Message\ConfirmationCodeSms;
use App\HelperMap\SmsClient\SmsClientInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ConfirmationCodeSmsHandler implements MessageHandlerInterface
{
    private SmsClientInterface $smsClient;

    public function __construct(SmsClientInterface $smsClient)
    {
        $this->smsClient = $smsClient;
    }

    public function __invoke(ConfirmationCodeSms $message): void
    {
        $this->smsClient->sendMessage($message->getPhone(), 'diaHelper code: ' . $message->getCode());
    }
}
