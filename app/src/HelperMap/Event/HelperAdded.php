<?php

declare(strict_types=1);

namespace App\HelperMap\Event;

use App\HelperMap\Entity\Helper;

class HelperAdded implements HelperStatusChangedInterface
{
    public const NAME = 'helper.added';

    private Helper $helper;

    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    public function getHelper(): Helper
    {
        return $this->helper;
    }
}
