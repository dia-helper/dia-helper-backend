<?php

namespace App\HelperMap\Event;

use App\HelperMap\Entity\Helper;

interface HelperStatusChangedInterface
{
    public function getHelper(): Helper;
}
