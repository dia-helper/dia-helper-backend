<?php

declare(strict_types=1);

namespace App\HelperMap\Message;

class ConfirmationCodeSms
{
    private string $phone;

    private string $code;

    public function __construct(string $phone, string $code)
    {
        $this->phone = $phone;
        $this->code = $code;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
