<?php

declare(strict_types=1);

namespace App\HelperMap\Controller;

use App\HelperMap\Entity\Helper;
use App\HelperMap\Event\HelperDeletionRequested;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RemoveHelperController extends AbstractController
{
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route(path="/helper-map/{phone}", methods={"DELETE"})
     */
    public function __invoke(Helper $helper): JsonResponse
    {
        $this->eventDispatcher->dispatch(new HelperDeletionRequested($helper), HelperDeletionRequested::NAME);

        return $this->json($helper->getId());
    }
}
