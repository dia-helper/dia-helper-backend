<?php

declare(strict_types=1);

namespace App\HelperMap\Controller;

use App\HelperMap\Entity\Helper;
use App\HelperMap\Repository\HelperRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class HelperListController extends AbstractController
{
    private HelperRepositoryInterface $helperRepository;

    private NormalizerInterface $serializer;

    public function __construct(
        HelperRepositoryInterface $helperRepository,
        NormalizerInterface $serializer
    ) {
        $this->helperRepository = $helperRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(path="/helper-map", methods={"GET"})
     */
    public function __invoke(): JsonResponse
    {
        $helperList = $this->helperRepository->findBy(['isActive' => true]);

        return $this->json($this->serializer->normalize($helperList, null, ['groups' => ['list']]));
    }
}
