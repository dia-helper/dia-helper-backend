<?php

declare(strict_types=1);

namespace App\HelperMap\Controller;

use App\HelperMap\Dto\ConfirmData;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class RemoveConfirmController extends AbstractController
{
    private EntityManagerInterface $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @Route(path="/helper-map/{id}/remove-confirm", methods={"PUT"})
     */
    public function __invoke(ConfirmData $confirmData): JsonResponse
    {
        $helper = $confirmData->getHelper();

        foreach ($helper->getConfirmationCode() as $code) {
            if ($code->isConfirmed()) {
                continue;
            }

            if ($code->getCode() === $confirmData->getCode()) {
                $code->confirmRemove();
                $this->em->persist($code);
                $this->em->flush();

                return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
            }
        }

        return new JsonResponse('incorrect code', JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
