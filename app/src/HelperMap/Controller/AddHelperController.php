<?php

declare(strict_types=1);

namespace App\HelperMap\Controller;

use App\HelperMap\Entity\Helper;
use App\HelperMap\Event\HelperAdded;
use App\HelperMap\Repository\HelperRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AddHelperController extends AbstractController
{
    private EntityManagerInterface $em;

    private HelperRepositoryInterface $helperRepository;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        EntityManagerInterface $em,
        HelperRepositoryInterface $helperRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->helperRepository = $helperRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route(path="/helper-map", methods={"POST"})
     */
    public function __invoke(Helper $rawHelper): JsonResponse
    {
        $helper = $this->helperRepository->findOneBy(['phone' => $rawHelper->getPhone()]);
        if ($helper && $helper->isActive()) {
            return new JsonResponse('phone already exists', JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
        if ($helper) {
            $this->em->remove($helper);
            $this->em->flush();
        }

        $this->em->persist($rawHelper);
        $this->em->flush();
        $this->eventDispatcher->dispatch(new HelperAdded($rawHelper), HelperAdded::NAME);

        return $this->json($rawHelper->getId());
    }
}
