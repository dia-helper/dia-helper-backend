<?php

declare(strict_types=1);

namespace App\HelperMap\Dto;

use App\HelperMap\Entity\Helper;

class ConfirmData
{
    private Helper $helper;

    private string $code;

    public function __construct(Helper $helper, string $code)
    {
        $this->helper = $helper;
        $this->code = $code;
    }

    public function getHelper(): Helper
    {
        return $this->helper;
    }

    public function getCode(): string
    {
        return $this->code;
    }
}
