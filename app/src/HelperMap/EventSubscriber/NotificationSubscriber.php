<?php

declare(strict_types=1);

namespace App\HelperMap\EventSubscriber;

use App\HelperMap\Entity\HelperConfirmationCode;
use App\HelperMap\Event\HelperAdded;
use App\HelperMap\Event\HelperDeletionRequested;
use App\HelperMap\Event\HelperStatusChangedInterface;
use App\HelperMap\Message\ConfirmationCodeSms;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class NotificationSubscriber implements EventSubscriberInterface
{
    private EntityManagerInterface $em;

    private MessageBusInterface $messageBus;

    public function __construct(EntityManagerInterface $em, MessageBusInterface $messageBus)
    {
        $this->em = $em;
        $this->messageBus = $messageBus;
    }

    public static function getSubscribedEvents()
    {
        return [
            HelperAdded::NAME => 'sendConfirmationCode',
            HelperDeletionRequested::NAME => 'sendConfirmationCode',
        ];
    }

    public function sendConfirmationCode(HelperStatusChangedInterface $event): void
    {
        $helper = $event->getHelper();
        $confirmationCode = new HelperConfirmationCode($helper, (string) rand(1000, 9999));
        $this->em->persist($confirmationCode);
        $this->em->flush();

        $message = new ConfirmationCodeSms('+' . $helper->getPhone(), $confirmationCode->getCode());
        $this->messageBus->dispatch($message);
    }
}
