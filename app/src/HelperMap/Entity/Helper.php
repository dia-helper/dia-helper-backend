<?php

declare(strict_types=1);

namespace App\HelperMap\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Helper
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list"})
     */
    private int $id;

    /**
     * @ORM\Column()
     * @Groups({"list"})
     */
    private string $name;

    /**
     * @ORM\Column(unique=true)
     * @Groups({"list"})
     */
    private string $phone;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=16)
     * @Groups({"list"})
     */
    private string $lat;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=16)
     * @Groups({"list"})
     */
    private string $lng;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isActive = false;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $updatedAt;

    /**
     * @var HelperConfirmationCode[]
     * @ORM\OneToMany(targetEntity="App\HelperMap\Entity\HelperConfirmationCode", mappedBy="helper", cascade="remove")
     */
    private iterable $confirmationCode;

    public function __construct(string $name, string $phone, string $lat, string $lng)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getLat(): string
    {
        return $this->lat;
    }

    public function getLng(): string
    {
        return $this->lng;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return HelperConfirmationCode[]
     */
    public function getConfirmationCode(): iterable
    {
        return $this->confirmationCode;
    }

    public function activate(): void
    {
        $this->isActive = true;
    }

    public function deactivate(): void
    {
        $this->isActive = false;
    }

    /**
     * @ORM\PrePersist
     */
    public function setTimestampsValue(): void
    {
        $dateTimeNow = new \DateTimeImmutable();

        $this->updatedAt = $dateTimeNow;
        $this->createdAt = $dateTimeNow;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateTimestampValue(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
