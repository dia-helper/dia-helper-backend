<?php

declare(strict_types=1);

namespace App\HelperMap\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class HelperConfirmationCode
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\HelperMap\Entity\Helper", inversedBy="confirmationCode", cascade="persist")
     */
    private Helper $helper;

    /**
     * @ORM\Column()
     */
    private string $code;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isConfirmed = false;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private \DateTimeImmutable $updatedAt;

    public function __construct(Helper $helper, string $code)
    {
        $this->helper = $helper;
        $this->code = $code;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getHelper(): Helper
    {
        return $this->helper;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function isConfirmed(): bool
    {
        return $this->isConfirmed;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function confirmAdding(): void
    {
        $this->isConfirmed = true;
        $this->helper->activate();
    }

    public function confirmRemove(): void
    {
        $this->isConfirmed = true;
        $this->helper->deactivate();
    }

    /**
     * @ORM\PrePersist
     */
    public function setTimestampsValue(): void
    {
        $dateTimeNow = new \DateTimeImmutable();

        $this->updatedAt = $dateTimeNow;
        $this->createdAt = $dateTimeNow;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateTimestampValue(): void
    {
        $this->updatedAt = new \DateTimeImmutable();
    }
}
