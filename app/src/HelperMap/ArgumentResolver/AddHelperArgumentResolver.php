<?php

declare(strict_types=1);

namespace App\HelperMap\ArgumentResolver;

use App\HelperMap\Entity\Helper;
use App\HelperMap\Exception\ArgumentResolverException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class AddHelperArgumentResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return Helper::class === $argument->getType() && Request::METHOD_POST === $request->getMethod();
    }

    /**
     * @return iterable<Helper>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        try {
            $data = json_decode($request->getContent(), true);

            yield new Helper($data['name'], $data['phone'], $data['lat'], $data['lng']);
        } catch (\TypeError $e) {
            throw new ArgumentResolverException('enter helper data');
        }
    }
}
