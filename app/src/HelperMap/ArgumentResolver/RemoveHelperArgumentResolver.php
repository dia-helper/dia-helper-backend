<?php

declare(strict_types=1);

namespace App\HelperMap\ArgumentResolver;

use App\HelperMap\Entity\Helper;
use App\HelperMap\Exception\ArgumentResolverException;
use App\HelperMap\Repository\HelperRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class RemoveHelperArgumentResolver implements ArgumentValueResolverInterface
{
    private HelperRepositoryInterface $helperRepository;

    public function __construct(
        HelperRepositoryInterface $helperRepository
    ) {
        $this->helperRepository = $helperRepository;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return Helper::class === $argument->getType() && Request::METHOD_DELETE === $request->getMethod();
    }

    /**
     * @return iterable<Helper>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $phone = $request->attributes->get('phone');
        $helper = $this->helperRepository->findOneBy(['phone' => $phone, 'isActive' => true]);
        if (!$helper) {
            throw new ArgumentResolverException('phone not found');
        }

        yield $helper;
    }
}
