<?php

declare(strict_types=1);

namespace App\HelperMap\ArgumentResolver;

use App\HelperMap\Dto\ConfirmData;
use App\HelperMap\Exception\ArgumentResolverException;
use App\HelperMap\Repository\HelperRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class AddingHelperConfirmArgumentResolver implements ArgumentValueResolverInterface
{
    private HelperRepositoryInterface $helperRepository;

    public function __construct(
        HelperRepositoryInterface $helperRepository
    ) {
        $this->helperRepository = $helperRepository;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConfirmData::class === $argument->getType() && Request::METHOD_PUT === $request->getMethod();
    }

    /**
     * @return iterable<ConfirmData>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $id = $request->attributes->get('id');
        $helper = $this->helperRepository->find($id);
        if (!$helper) {
            throw new ArgumentResolverException('phone not found');
        }
        $data = json_decode($request->getContent(), true);
        $code = $data['code'];
        if (empty($code)) {
            throw new ArgumentResolverException('enter the confirmation code');
        }

        yield new ConfirmData($helper, $code);
    }
}
