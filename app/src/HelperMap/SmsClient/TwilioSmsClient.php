<?php

declare(strict_types=1);

namespace App\HelperMap\SmsClient;

use Twilio\Rest\Client;

class TwilioSmsClient implements SmsClientInterface
{
    private const TWILIO_NUMBER = '+18507547172';

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function sendMessage(string $phone, string $message): void
    {
        $this->client->messages->create(
            $phone,
            [
                'from' => self::TWILIO_NUMBER,
                'body' => $message,
            ]
        );
    }
}
