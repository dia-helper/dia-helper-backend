<?php

namespace App\HelperMap\SmsClient;

interface SmsClientInterface
{
    public function sendMessage(string $phone, string $message): void;
}
