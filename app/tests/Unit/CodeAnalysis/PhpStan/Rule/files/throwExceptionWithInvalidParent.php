<?php

try {
    throw new \Exception('e');
} catch (\Exception $e) {
}

try {
    throw new \RuntimeException('e');
} catch (\RuntimeException $e) {
}

try {
    throw new \RangeException('e');
} catch (\RangeException $e) {
}

try {
    throw new \TypeError('e');
} catch (\TypeError $e) {
}
