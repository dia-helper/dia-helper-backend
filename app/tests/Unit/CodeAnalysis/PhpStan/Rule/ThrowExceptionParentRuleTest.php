<?php

namespace App\Tests\Unit\CodeAnalysis\PhpStan\Rule;

use App\CodeAnalysis\PhpStan\Rule\ThrowExceptionParentRule;
use PHPStan\Rules\Rule;
use PHPStan\Testing\RuleTestCase;

/**
 * @extends RuleTestCase<ThrowExceptionParentRule>
 */
class ThrowExceptionParentRuleTest extends RuleTestCase
{
    private const ERROR_TEXT = 'Invalid Exception parent. You can only throw an exception of one of the following types: LogicException, UnexpectedValueException';

    private const TEST_FILE_PATH = __DIR__ . '/files';

    protected function getRule(): Rule
    {
        return new ThrowExceptionParentRule([\LogicException::class, \UnexpectedValueException::class]);
    }

    public function testThrowNoErrors(): void
    {
        $this->analyse([self::TEST_FILE_PATH . '/throwExceptionWithValidParent.php'], []);
    }

    public function testThrowExceptionWithErrors(): void
    {
        $this->analyse([self::TEST_FILE_PATH . '/throwExceptionWithInvalidParent.php'], [
            [self::ERROR_TEXT, 4],
            [self::ERROR_TEXT, 9],
            [self::ERROR_TEXT, 14],
            [self::ERROR_TEXT, 19],
        ]);
    }
}
