<?php

declare(strict_types=1);

namespace App\Tests\Unit\Open\Service;

use App\Open\Dto\ChartDataDto;
use App\Open\Exception\FileExtractException;
use App\Open\Service\ArchiverInterface;
use App\Open\Service\XdrippCsvDataPreparer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class XdrippCsvDataPreparerTest extends TestCase
{
    private const ORIGIN_FILE_PATH = '/files/xdripp.csv';
    private const TEMP_FILE_PATH = '/files/xdripp_temp.csv';
    private const MIN_VALUE = 137;
    private const MAX_VALUE = 178;
    private const COUNT_VALUE = 4;

    private XdrippCsvDataPreparer $service;

    public function setUp(): void
    {
        $unzipCallback = function (UploadedFile $file): string {
            if ($filePath = $file->getRealPath()) {
                return $filePath;
            }

            throw new FileExtractException('file path extract error');
        };
        $archiver = $this->getMockBuilder(ArchiverInterface::class)
            ->getMock();
        $archiver->method('getUnzippedFilePath')
            ->willReturnCallback($unzipCallback);

        $this->service = new XdrippCsvDataPreparer($archiver);
    }

    public function testPrepareData(): void
    {
        $filePath = __DIR__ . self::ORIGIN_FILE_PATH;
        $testFilePath = __DIR__ . self::TEMP_FILE_PATH;
        copy($filePath, $testFilePath);
        self::assertFileExists($testFilePath);

        $file = new UploadedFile($testFilePath, basename($testFilePath));
        $result = $this->service->prepareData($file);
        self::assertNotEmpty($result);
        self::assertInstanceOf(ChartDataDto::class, $result);
        self::assertCount(self::COUNT_VALUE, $result->getLabels());
        self::assertCount(self::COUNT_VALUE, $result->getData());
        self::assertEquals(self::MIN_VALUE, min($result->getData()));
        self::assertEquals(self::MAX_VALUE, max($result->getData()));
    }

    public function testGetAcceptableMimeType(): void
    {
        $mimeType = $this->service->getAcceptableMimeType();
        self::assertNotEmpty($mimeType);
        self::assertIsArray($mimeType);
        self::assertIsString(array_shift($mimeType));
    }
}
