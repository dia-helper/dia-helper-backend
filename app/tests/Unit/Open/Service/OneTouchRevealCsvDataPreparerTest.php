<?php

declare(strict_types=1);

namespace App\Tests\Unit\Open\Service;

use App\Open\Dto\ChartDataDto;
use App\Open\Service\OneTouchRevealCsvDataPreparer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class OneTouchRevealCsvDataPreparerTest extends TestCase
{
    private const FILE_PATH = '/files/one-touch-reveal.csv';
    private const MIN_VALUE = 18;
    private const MAX_VALUE = 183.6;
    private const COUNT_VALUE = 4;
    private const VALUE_FROM_MMOL_L = 82.8;

    private OneTouchRevealCsvDataPreparer $service;

    protected function setUp(): void
    {
        $this->service = new OneTouchRevealCsvDataPreparer();
    }

    public function testPrepareData(): void
    {
        $filePath = __DIR__ . self::FILE_PATH;
        self::assertFileExists($filePath);

        $file = new UploadedFile($filePath, basename($filePath));
        $result = $this->service->prepareData($file);
        self::assertNotEmpty($result);
        self::assertInstanceOf(ChartDataDto::class, $result);
        self::assertCount(self::COUNT_VALUE, $result->getLabels());
        self::assertCount(self::COUNT_VALUE, $result->getData());
        self::assertEquals(self::MIN_VALUE, min($result->getData()));
        self::assertEquals(self::MAX_VALUE, max($result->getData()));
        self::assertContains(self::VALUE_FROM_MMOL_L, $result->getData());
    }

    public function testGetAcceptableMimeType(): void
    {
        $mimeType = $this->service->getAcceptableMimeType();
        self::assertNotEmpty($mimeType);
        self::assertIsArray($mimeType);
        self::assertIsString(array_shift($mimeType));
    }
}
