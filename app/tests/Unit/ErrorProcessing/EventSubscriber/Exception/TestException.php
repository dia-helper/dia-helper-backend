<?php

declare(strict_types=1);

namespace App\Tests\Unit\ErrorProcessing\EventSubscriber\Exception;

use App\ErrorProcessing\Exception\ClientException;

class TestException extends ClientException
{
}
