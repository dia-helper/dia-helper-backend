<?php

declare(strict_types=1);

namespace App\Tests\Unit\ErrorProcessing\EventSubscriber;

use App\ErrorProcessing\EventSubscriber\ExceptionSubscriber;
use App\Tests\Unit\ErrorProcessing\EventSubscriber\Exception\TestException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

final class ExceptionSubscriberTest extends TestCase
{
    private const TEST_ERROR_MESSAGE = 'test message';

    private const TEST_SERVER_ERROR = 'server error';

    private ExceptionSubscriber $exceptionSubscriber;

    public function setUp(): void
    {
        $this->exceptionSubscriber = new ExceptionSubscriber();
    }

    public function testClientException(): void
    {
        $event = new ExceptionEvent(
            $this->createMock(HttpKernelInterface::class),
            $this->createMock(Request::class),
            HttpKernelInterface::MASTER_REQUEST,
            new TestException(self::TEST_ERROR_MESSAGE)
        );
        $this->exceptionSubscriber->processException($event);

        $response = $event->getResponse();
        self::assertNotEmpty($response);
        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        self::assertEquals(json_encode(self::TEST_ERROR_MESSAGE), $response->getContent());
    }

    public function testUnknownException(): void
    {
        $event = new ExceptionEvent(
            $this->createMock(HttpKernelInterface::class),
            $this->createMock(Request::class),
            HttpKernelInterface::MASTER_REQUEST,
            new \Exception(self::TEST_SERVER_ERROR)
        );
        $this->exceptionSubscriber->processException($event);

        $response = $event->getResponse();
        self::assertNotEmpty($response);
        self::assertInstanceOf(JsonResponse::class, $response);
        self::assertEquals(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode());
        self::assertEquals(json_encode(self::TEST_SERVER_ERROR), $response->getContent());
    }

    public function testGetSubscribedEvents(): void
    {
        $subscribedEvents = ExceptionSubscriber::getSubscribedEvents();
        self::assertNotEmpty($subscribedEvents);
        self::assertIsArray($subscribedEvents);
        self::assertIsString(array_shift($subscribedEvents));
    }
}
